
import { renderFavoriteStatus } from '../events/favorites-events.js';
import { renderStarStatus } from '../events/star-events.js';
export const toSingleViewTrending = (gif) => `
        <div class="img-div" style="background-color: #47eb31;">
            <img src="${gif.images.downsized.url}" >
            <div class="info">
            ${renderFavoriteStatus(gif.id)}
                <a href="#"><p id="${gif.id}" class="view-detail">View details</p></a>
                ${renderStarStatus(gif.id)}
            </div>
        </div>
        `;