export const gifDetailsView = (details) => `
      <div class="details-container">
      <btn class="close-btn" aria-label="Dismiss alert" type="btn" data-close>
        <i class="fa-solid fa-xmark fa-lg"></i>
      </btn>
      <div class="gif-details">
      <div class="gif">
        <img src="${details.data.images.downsized.url}" alt="${details.data.title}">
      </div>
      <div class="text-container">
            <p><strong>Title:</strong> ${details.data.title}</p>
            <p><strong>Type:</strong> ${details.data.type}</p>
            <p><strong>Uploaded by:</strong> ${details.data.username}</p>
            <p><strong>Rating:</strong> ${details.data.rating} </p>
            <p><strong>Created on:</strong> ${details.data.import_datetime}</p>
            <p><strong>id:</strong> ${details.data.id}</p>
            <p><strong>URL:</strong> ${details.data.url}</p>
            <p><strong>Source:</strong> ${details.data.source_tld}</p>
          </div>
       </div>
  </div>`;
