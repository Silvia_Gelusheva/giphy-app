
import { renderFavoriteStatus } from '../events/favorites-events.js';
import { renderStarStatus } from '../events/star-events.js';

export const toFavoritesView = (el) => 
  `
<div class="img-div" style="background-color: #d628cd;">
  <img src="${el.images.downsized.url}" alt="">
    <div class="info">
    ${renderFavoriteStatus(el.id)}
      <a href="#"><p id="${el.id}" class="view-detail">View details</p></a>
      ${renderStarStatus(el.id)}
    </div>
</div>
`;