const apiKey = "gCEJwZetdOlkZIo1Ol85kbRr0LZXjXA0";
export const limit = 20
export const limit2 = 70

export const FULL_HEART = '❤';

export const EMPTY_HEART = '♡';

export const EMPTY_STAR = '☆';

export const FULL_STAR = '★';



export const gifByIdUrl = `https://api.giphy.com/v1/gifs?api_key=${apiKey}&ids=`;
export const randomUrl = `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`;
export const trendingUrl = `https://api.giphy.com/v1/gifs/trending?api_key=${apiKey}&limit=${limit}`
export const trendingOffsetUrl = `https://api.giphy.com/v1/gifs/trending?api_key=${apiKey}&limit=${limit2}&offset=50`
export const searchUrl = `https://api.giphy.com/v1/gifs/search?api_key=${apiKey}&limit=${limit}&q=`

export const gifDetailsUrlHead = `https://api.giphy.com/v1/gifs/`;

export const apiKeyUrlTail = `?api_key=${apiKey}`;

export const detailedURL = '';