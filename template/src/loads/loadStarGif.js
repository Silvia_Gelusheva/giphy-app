import { gifByIdUrl } from "../common/constants.js"
import { getStar } from "../events/star-events.js";
import {randomUrl} from "../common/constants.js"

export function loadStarGif(){

    const star = getStar()
    
    if(star.length === 0){
        fetch(randomUrl)
        .then(response => response.json())
        .then(content => {
            document.querySelector('.star-gif').innerHTML = 
            `
            <img src="${content.data.images.downsized.url}" alt="">
            `
        }).catch(err => {
            console.error(err);
            })
    }else{
        fetch(`${gifByIdUrl}${star.join('')}`)
        .then(response => response.json())
        .then(content => {
        document.querySelector('.star-gif').innerHTML =
        `
          <img src="${content.data[0].images.downsized.url}" alt="">
        `
        })
        .catch(err => {
        console.error(err);
        })

    }

 
}

 
