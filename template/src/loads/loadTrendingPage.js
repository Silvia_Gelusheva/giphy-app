import { trendingUrl } from '../common/constants.js';

export function loadTrendingImages() {
  const trendingPageGifs = fetch(trendingUrl)
    .then((response) => response.json())
    .catch((err) => {
      console.error(err);
    });
    return trendingPageGifs
}

