
import { toggleFavoriteStatus } from './events/favorites-events.js';
import { toggleStarStatus } from './events/star-events.js';
import { loadStarGif } from './loads/loadStarGif.js';
import { gifDetails } from './loads/loadGifDetails.js';
import { hideDetails } from './events/hideDetails.js';
import { renderFavoritesPage, renderHomePage, renderSearchPage, renderTrendingPage, renderUploadsPage } from './events/AllEvents.js'

document.addEventListener('DOMContentLoaded', () => {
  renderHomePage();
  loadStarGif();

  document.addEventListener('click', (event) => {
    if (event.target.classList.contains('Home')) {
      renderHomePage();
    }
    if (event.target.classList.contains('Trending')) {
      renderTrendingPage();
    }
    if (event.target.classList.contains('Favorites')) {
      renderFavoritesPage();
    }

    if (event.target.classList.contains('Uploads')) {
      renderUploadsPage();
    }

    if (event.target.classList.contains('favorite')) {
      toggleFavoriteStatus(event.target.getAttribute('data-gif-id'));
    }

    if (event.target.classList.contains('star')) {
      toggleStarStatus(event.target.getAttribute('data-star-id'));
      loadStarGif();
    }

    if (event.target.classList.contains('view-detail')) {
      gifDetails(event.target.getAttribute('id'));
    }

    if (event.target.classList.contains('close-btn')) {
      hideDetails();
    }
  });
  document.getElementById('btnSearch').addEventListener('click', (ev) => {
    ev.preventDefault();
    renderSearchPage();
  });
});
